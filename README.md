My CSS Sandbox
==============


Installation
-------------

Ruby needs to be installed. [rvm](https://rvm.io) is a good tool to mange ruby version with.

* Install [SASS](http://sass-lang.com): `gem update system; gem install sass`
* Install [Compass](http://compass-style.org): `gem install compass`
* Install [bourbon](http://bourbon.io): `gem install bourbon`
* Install [Susy](http://susy.oddbird.net): `gem install susy`

More tools:

* [stats](gem install css_parser): `compass stats`
* [Breakpoint](http://breakpoint-sass.com)
* [Vertical rhythm](http://compass-style.org/reference/compass/typography/vertical_rhythm/)


Create project
--------------

* Use bourbon: `bourbon install`
* Use compass: `compass create`
* Use Susy: `compass create --using susy <project name>`
 * Alternatively, add Susy to a current project: `compass install susy`
* Use stats: `gem install css_parser`


Sass packages - [Sache](http://www.sache.in)
-------------------------------------------


