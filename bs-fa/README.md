Install:

    bower install
    ln -s bower_components/fontawesome/fonts fonts

Compile:


    export SASS_PATH="../bower_components/bootstrap-sass/assets/stylesheets:../bower_components/fontawesome/scss"
    mkdir css
    cd scss; sass style.scss ../css/style.css
