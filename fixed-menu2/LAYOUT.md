CSS Layout design
=================

CSS Box model
-------------

```
    +---------------------------+   ^
    |          margin           |   |
    |  +---------------------+  |
    |  |       border        |  |
    |  |  +---------------+  |  |
    |  |  |    padding    |  |  |   height (box-sizing)
    |  |  |  +---------+  |  |  |
    |  |  |  | content |  |  |  |
    |  |  |  +---------+  |  |  |
    |  |  +---------------+  |  |
    |  +---------------------+  |   |
    +---------------------------+   v
```

margin, padding and border is set to 0 by the CSS reset.


Layout
------

```
    ROOT                                                        ROOT
                                                                height: 100vh;
    l-top; height: 5rems    l-content-header; padding: 2rems    l-middle; margin: -5rems;  l-content; padding: 5rems
    +------------           +----------                         +---------------           +-------------
    |                       |                                   |                          |
    |                       |                                   |                          |
    |                       |           +-----                  |                          |
    |                       +---------- +-----                  |                          |
    |                                                           |                          |
    +------------                                               |                          |     +--------
                                                                |                          |     | CONTENT
                                                                |                          |     |
                                                                |
    ROOT                                                        ...
    l-bottom; height: 5rems; background: ...                    |                          |     +--------
    +------------                                               |                          +--------------
    |                                                           |
    |                                                           |
    |                                                           |
    |                     +--------                             |
    |                     | l-menu                              |
    +------------         +--------                             +--------------- margin: -5rems
```
