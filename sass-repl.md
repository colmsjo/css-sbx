Example

    sass -i

    $a: ();
    $b: unquote('');
    $c: null;
    $d: (null);


    type-of($a) -> list
    type-of($b) -> string
    type-of($c) -> null
    type-of($d) -> null

    length($a) -> 0
    length($b) -> 1
    length($c) -> 1
